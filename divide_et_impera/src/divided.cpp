#include "divided.hpp"
using stockpile_type_c_it = std::deque<Package>::const_iterator;
/* STORAGE */

ElementId Package::_current_max_id = 1;


void Worker::process() {}
void Worker::receive_package(std::unique_ptr<Package>) {}
std::pair<ReceiverType, ElementId> Worker::verify_identity() { return std::make_pair(ReceiverType::Worker, Worker::get_id()); }
stockpile_type_c_it Worker::cbegin() const { return end(); }
stockpile_type_c_it Worker::begin() const { return end(); }
stockpile_type_c_it Worker::cend() const { return end(); }          ///do poprawy motzno
stockpile_type_c_it Worker::end() const { return begin(); }


std::pair<ReceiverType, ElementId> Storehouse::verify_identity() { return std::make_pair(ReceiverType::Storehouse, Storehouse::get_id()); }
void Storehouse::receive_package(std::unique_ptr<Package>) {}
stockpile_type_c_it Storehouse::cbegin() const { return begin(); }
stockpile_type_c_it Storehouse::begin() const { return end(); }
stockpile_type_c_it Storehouse::cend() const { return end(); }          ///do poprawy motzno
stockpile_type_c_it Storehouse::end() const { return begin(); }



void ReceiverPreferences::delete_receiver(IPackageReceiver *receiver) {

    auto found_receiver = _receiver_preferences_collection.find(receiver);
    if(found_receiver != _receiver_preferences_collection.end())
        _receiver_preferences_collection.erase(found_receiver);

    //scale_probability();   ??

}


void Factory::add_ramp(Ramp &ramp) { _ramps_list.add_node(ramp); }
void Factory::add_worker(Worker &worker) { _workers_list.add_node(worker); }
void Factory::add_storehouse(Storehouse &storehouse) { _storehouses_list.add_node(storehouse); }

NodeCollection<Ramp>::const_iterator Factory::find_ramp_by_id(ElementId id) const { return _ramps_list.find_by_id(id); }
Workers::const_iterator Factory::find_worker_by_id(ElementId id) const { return _workers_list.find_by_id(id); }
Storehouses::const_iterator Factory::find_storehouse_by_id(ElementId id) const { return _storehouses_list.find_by_id(id); }

void Factory::delete_ramp_by_id(ElementId id) { _ramps_list.remove_by_id(id); }


void Factory::delete_worker_by_id(ElementId id) {

    Worker deleted_worker = *(_workers_list.find_by_id(id));
    IPackageReceiver* deleted_worker_ptr = &deleted_worker;
    delete_connections(deleted_worker_ptr);
    _workers_list.remove_by_id(id);
}


void Factory::delete_storehouse_by_id(ElementId id) {

    Storehouse deleted_storehouse = (*_storehouses_list.find_by_id(id));
    IPackageReceiver* deleted_storehouse_ptr = &deleted_storehouse;
    delete_connections(deleted_storehouse_ptr);
    _storehouses_list.remove_by_id(id);
}



void Factory::delete_connections(IPackageReceiver* receiver_ptr) {

    for (auto& worker : _workers_list){
        std::map<IPackageReceiver*, ProbabilityGenerator> preferences_map = worker.receiver_preference.get_preferences_collection();
        auto search_for_connection = preferences_map.find(receiver_ptr);
        if(search_for_connection != preferences_map.end())
            worker.receiver_preference.delete_receiver(receiver_ptr);
    }

    for (auto& ramp : _ramps_list){
        std::map<IPackageReceiver*, ProbabilityGenerator> preferences_map = ramp.receiver_preference.get_preferences_collection();
        auto search_for_connection = preferences_map.find(receiver_ptr);
        if(search_for_connection != preferences_map.end())
            ramp.receiver_preference.delete_receiver(receiver_ptr);
    }
}

void consistency_error_thrower(const NodeCollection<PackageSender>& senders){

    std::vector<ReceiverPreferences> senders_receivers;
    std::ostringstream sender_error_message;

    std::string sender_type;        //wersja gdzie potrzeba metody identify_package_sender

    for(auto& sender : senders){
        if(sender.receiver_preference.get_preferences_collection().empty()){
            sender_error_message << sender_type << " #" << sender.get_id() << "nie posiada połączenia wyjściowego" << std::endl;
            throw std::runtime_error(sender_error_message.str());
        }
    }


}

bool Factory::verify_if_consistent() const {                //  VIOLATION OF DRY PRINCIPLE AS HELL

    //consistency_error_thrower(get_ramps_list());
    /*
    std::ostringstream worker_error_message;
    for(auto& worker : get_workers_list()){
        if(worker.receiver_preference.get_preferences_collection().empty()){
            worker_error_message << "Pracownik #" << worker.get_id()<< "nie posiada połączenia wyjściowego" << std::endl;
            throw std::runtime_error(worker_error_message.str());
        }
    }
    */

    return true;
}


std::vector<std::string> parse_line(std::string& string_line, char delimiter){

    std::vector<std::string> tokens;
    std::string token;

    std::istringstream token_stream(string_line);

    while (std::getline(token_stream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;

}


struct_elem_string load_factory_structure(std::istream& input_structure){         // zmień void na mape stringów

    if(input_structure){

        std::map<std::string, std::string> strings_map;
        std::string token;

        std::vector<std::string> str_vec;
        std::vector<std::string> str_vec_ef_parsed_elems;
        while(std::getline(input_structure,token)){
            if (token[0] != ';' && token.length()!=0){
                str_vec.push_back(token);
            }
        }

        for (const auto& elem : str_vec) {
            std::istringstream element(elem);
            std::string parsed_elem;
            while(std::getline(element, parsed_elem, ' ')){
                str_vec_ef_parsed_elems.push_back(parsed_elem);
            }
            for(unsigned int i =1; i<str_vec_ef_parsed_elems.size(); i++)
                strings_map[str_vec_ef_parsed_elems[0]] += str_vec_ef_parsed_elems[i]+' ';
            strings_map[str_vec_ef_parsed_elems[0]] +=';';


            str_vec_ef_parsed_elems.clear();
        }

        std::string links_str = strings_map["LINK"];
        std::string loading_ramps_str = strings_map["LOADING_RAMP"];
        std::string storehouses_str = strings_map["STOREHOUSE"];
        std::string workers_str = strings_map["WORKER"];

        links_str.resize(links_str.size()-2);
        loading_ramps_str.resize(loading_ramps_str.size()-2);
        storehouses_str.resize(storehouses_str.size()-2);
        workers_str.resize(workers_str.size()-2);

        struct_elem_string type_with_str_mapped;
        type_with_str_mapped[StructureElementType::Link]=links_str;
        type_with_str_mapped[StructureElementType::Worker]=workers_str;
        type_with_str_mapped[StructureElementType::Storehouse]=storehouses_str;
        type_with_str_mapped[StructureElementType::Ramp]=loading_ramps_str;

        return type_with_str_mapped;
    }
    else{
        std::cout << "No such file!" << std::endl;
        return struct_elem_string();
    }
}



std::vector<std::vector<std::string>> parse_to_separate_values(std::vector<std::string> vector_of_lines){

    std::vector<std::vector<std::string>> divided_values_vector;
    for (auto& elem : vector_of_lines)
        divided_values_vector.push_back(parse_line(elem,' '));
    std::vector<std::vector<std::string>> divided_values_without_equal_sign;

    return divided_values_vector;
}


Worker create_worker_from_parsed_values(std::vector<std::string> parsed_vals){
    int id = parsed_vals[0][3]-'0';
    int processing_time = parsed_vals[1][16]-'0';
    //char char_queue_type = parsed_vals[2][11];

    std::map<char, QueueType> char_to_queue_type_map;
    char_to_queue_type_map['L']=QueueType::LIFO;
    char_to_queue_type_map['F']=QueueType::FIFO;


    return Worker(id,processing_time);   ///    DO POPRAWY!!   DODAJ QUEUE_PTR DO KONSTRUKTORA
}


Storehouse create_storehouse_from_parsed_values(std::vector<std::string> parsed_vals){
    int id = parsed_vals[0][3]-'0';
    return Storehouse(id);
}

void save_factory_structure(struct_elem_string& loaded_lines){

    for (auto& elem : loaded_lines)
        std::cout << elem.second<<std::endl;

    std::cout<<std::endl;

    std::vector<std::vector<std::string>> stripped_lines;

    for (auto& elem : loaded_lines)
        stripped_lines.push_back(parse_line(elem.second,';'));

    std::vector<std::vector<std::string>> workers_values = parse_to_separate_values(stripped_lines[0]);
    std::vector<std::vector<std::string>> ramps_values = parse_to_separate_values(stripped_lines[1]);
    std::vector<std::vector<std::string>> storehouses_values = parse_to_separate_values(stripped_lines[2]);
    std::vector<std::vector<std::string>> links_values = parse_to_separate_values(stripped_lines[3]);


}






