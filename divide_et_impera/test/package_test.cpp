#include "gtest/gtest.h"
#include "divided.hpp"

TEST(TestPackage, testIds) {

    Package package1 = Package();
    Package package2 = Package();

    EXPECT_EQ(package1.get_id(), 1);
    EXPECT_EQ(package2.get_id(), 2);

}

