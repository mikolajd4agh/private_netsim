#include "gtest/gtest.h"
#include "divided.hpp"

TEST(TestFactory, testConsistencyVerification){


}

TEST(TestFactory, testReceiverRemoving){



    Storehouse test_storehouse(1);

    ReceiverPreferences test_worker_preferences( {{&test_storehouse,0.5}} );
    Worker test_worker(1,1,test_worker_preferences);

    ReceiverPreferences test_ramp_preferences( {{&test_worker,0.7}} );
    Ramp test_ramp(1,1,test_ramp_preferences);

    std::list<Ramp> ramps_list;
    std::list<Worker> workers_list;
    std::list<Storehouse> storehouses_list;

    ramps_list.push_back(test_ramp);
    workers_list.push_back(test_worker);
    storehouses_list.push_back(test_storehouse);

    NodeCollection<Ramp> test_ramps(ramps_list);
    NodeCollection<Worker> test_workers(workers_list);

    NodeCollection<Storehouse> test_storehouses(storehouses_list);

    Factory test_factory(test_ramps, test_workers, test_storehouses);

    test_factory.delete_worker_by_id(1);


    EXPECT_EQ(test_factory.get_workers_list().get_nodes_collection(), std::list<Worker>());

    //using preferences_t = std::map<IPackageReceiver*, ProbabilityGenerator>;
    //EXPECT_EQ(test_factory.get_ramps_list().find_by_id(1)->receiver_preference.get_preferences_collection(), preferences_t());

}