#include "gtest/gtest.h"
#include "divided.hpp"

TEST(TestNodeCollection, testFindingById){      /// czy to nie powinno być testowane na ogólnym typie Node???

    Storehouse test_receiver = Storehouse(1);
    IPackageReceiver* receiver_ptr = &test_receiver;
    std::map<IPackageReceiver*, double> test_receiver_preferences_map;
    test_receiver_preferences_map = {{receiver_ptr,0.5}};
    ReceiverPreferences test_receiver_preferences = ReceiverPreferences(test_receiver_preferences_map);

    Ramp ramp1 = Ramp(1,1,test_receiver_preferences);
    Ramp ramp2 = Ramp(2,1,test_receiver_preferences);
    std::list<Ramp> ramps_collection;
    ramps_collection.push_back(ramp1);
    ramps_collection.push_back(ramp2);

    NodeCollection<Ramp> test_node_collection = NodeCollection(ramps_collection);

    EXPECT_EQ(*test_node_collection.find_by_id(2), ramp2);

}


TEST(TestNodeCollection, testRemovingById){      /// czy to nie powinno być testowane na ogólnym typie Node???

    Storehouse test_receiver = Storehouse(1);
    IPackageReceiver* receiver_ptr = &test_receiver;
    std::map<IPackageReceiver*, double> test_receiver_preferences_map;
    test_receiver_preferences_map = {{receiver_ptr,0.5}};
    ReceiverPreferences test_receiver_preferences = ReceiverPreferences(test_receiver_preferences_map);

    Ramp ramp1 = Ramp(1,1,test_receiver_preferences);
    Ramp ramp2 = Ramp(2,1,test_receiver_preferences);
    std::list<Ramp> ramps_collection;
    ramps_collection.push_back(ramp1);
    ramps_collection.push_back(ramp2);

    NodeCollection<Ramp> test_node_collection = NodeCollection(ramps_collection);
    test_node_collection.remove_by_id(1);

    std::list<Ramp> one_ramp_only;
    one_ramp_only.push_back(ramp2);

    EXPECT_EQ(test_node_collection.get_nodes_collection(), one_ramp_only);

}