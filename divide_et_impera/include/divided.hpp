#ifndef DIVIDE_ET_IMPERA_DIVIDED_HPP
#define DIVIDE_ET_IMPERA_DIVIDED_HPP

#include <cstdint>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <functional>
#include <iterator>
#include <memory>
#include <queue>
#include <list>
#include <set>
#include <deque>
#include <map>
#include <ostream>
#include <fstream>
#include <istream>
#include <algorithm>

/* PRIMITIVE */

using ElementId = int;
using Time = int;
using TimeOffset = int;
using ProbabilityGenerator = double;

/* PRIMITIVE */


/* STORAGE */

enum class QueueType{
    FIFO,
    LIFO
};


class Package {
public:
    Package() : _package_id(_current_max_id) { _current_max_id++; }
    ~Package() = default;
    ElementId get_id(){ return _package_id; }
private:
    static ElementId _current_max_id;
    ElementId _package_id;
};


class IPackageStockpile{
public:
    using stockpile_type_c_it = std::deque<Package>::const_iterator;

    virtual ~IPackageStockpile(){}
    virtual void put_package_into_stockpile(std::unique_ptr<Package>)=0;
    virtual stockpile_type_c_it find_package_by_id(ElementId package_id)=0;

    virtual stockpile_type_c_it begin()=0;
    virtual stockpile_type_c_it end()=0;
    virtual stockpile_type_c_it cbegin()=0;
    virtual stockpile_type_c_it cend()=0;
};


class IPackageQueue : public IPackageStockpile{
public:
    virtual ~IPackageQueue(){}
    virtual std::unique_ptr<Package> pull_package_from_queue()=0;
    virtual QueueType identify_queue_type()=0;
};


class PackageQueue : public IPackageQueue {
public:
    PackageQueue(std::deque<Package>& package_queue, QueueType queue_type) : _package_queue(package_queue), _queue_type(queue_type) {}
    void put_package_into_stockpile(std::unique_ptr<Package> package_ptr) override;
    std::unique_ptr<Package> pull_package_from_queue() override;
    stockpile_type_c_it find_package_by_id(ElementId package_id) override;
    QueueType identify_queue_type() override;
    ~PackageQueue() = default;

private:
    std::deque <Package>_package_queue;
    QueueType _queue_type;
};

/* STORAGE */


/* NODES */

enum class ReceiverType{
    Worker,
    Storehouse
};


class IPackageReceiver{
public:
    using stockpile_type_c_it = std::deque<Package>::const_iterator;
    virtual ~IPackageReceiver(){}

    virtual void receive_package(std::unique_ptr<Package>)=0;
    virtual std::pair<ReceiverType,ElementId> verify_identity()=0;

    virtual stockpile_type_c_it begin() const =0;
    virtual stockpile_type_c_it end()const=0;
    virtual stockpile_type_c_it cbegin()const=0;
    virtual stockpile_type_c_it cend()const=0;

};


class ReceiverPreferences{
    using preferences_t = std::map<IPackageReceiver*, ProbabilityGenerator>;
    using const_iterator = preferences_t::const_iterator;
    using iterator = preferences_t::iterator;
public:
    explicit ReceiverPreferences(preferences_t receiver_preferences) : _receiver_preferences_collection(receiver_preferences){}
    friend bool operator== (const ReceiverPreferences& pref1, const ReceiverPreferences& pref2) { return pref1.get_preferences_collection()==pref2.get_preferences_collection();}
    IPackageReceiver* pick_receiver();
    void add_receiver(IPackageReceiver* receiver);
    void delete_receiver(IPackageReceiver* receiver);
    preferences_t get_preferences_collection () const { return _receiver_preferences_collection; }

    const_iterator cbegin() const { return _receiver_preferences_collection.cbegin(); }
    const_iterator cend() const { return _receiver_preferences_collection.cend(); }
    const_iterator begin() const { return _receiver_preferences_collection.cbegin(); }
    const_iterator end() const { return _receiver_preferences_collection.cend(); }
private:
    preferences_t _receiver_preferences_collection;
    void scale_probability(preferences_t receiver_preferences);
};


class PackageSender{
public:
    explicit PackageSender(ReceiverPreferences receiver_preferences=ReceiverPreferences(std::map<IPackageReceiver*, ProbabilityGenerator>{}), std::optional<Package> package_buffer=std::nullopt) : receiver_preference(std::move(receiver_preferences)), _package_buffer(package_buffer){}
    ~PackageSender()= default;
    ReceiverPreferences receiver_preference;
    void send_package();
    virtual ElementId get_id() const { return -1; };             //do poprawy ??
    std::optional<Package> get_package_buffer() const { return _package_buffer; }
private:
    std::optional<Package> _package_buffer;
};


class Worker : public PackageSender, public IPackageReceiver {
public:
    Worker(ElementId worker_id, Time processing_time, ReceiverPreferences& receiver_preferences, std::unique_ptr<IPackageQueue> queue_type) : PackageSender(receiver_preferences), _worker_id(worker_id), _processing_time(processing_time), _workers_queue_ptr(std::move(queue_type)){}
    Worker(ElementId worker_id, Time processing_time, ReceiverPreferences& receiver_preferences) : PackageSender(receiver_preferences), _worker_id(worker_id), _processing_time(processing_time), _workers_queue_ptr(nullptr){}
    Worker(ElementId worker_id, Time processing_time, std::unique_ptr<IPackageQueue> queue_type=nullptr) : PackageSender(), _worker_id(worker_id), _processing_time(processing_time), _workers_queue_ptr(std::move(queue_type)){}

    /// DO POPRAWY! -> trzeba dodać przekazanie _worker_queue_ptr w konstruktorze kopiującym
    Worker(const Worker& orig) : PackageSender(orig.receiver_preference), _worker_id(orig._worker_id), _processing_time(orig._processing_time){}

    //Worker(Worker && orig) : PackageSender(orig.receiver_preference), _worker_id(orig._worker_id), _processing_time(orig._processing_time), _workers_queue_ptr(std::move(orig._workers_queue_ptr)){}   //może się przydać

    friend bool operator== (const Worker& wrk1, const Worker& wrk2) { return( wrk1.get_id()==wrk2.get_id() && wrk1.get_processing_time()==wrk2.get_processing_time() && wrk1.receiver_preference==wrk2.receiver_preference && wrk1._workers_queue_ptr==wrk2._workers_queue_ptr ); }
    void process();
    ElementId get_id() const override { return _worker_id; }
    Time get_processing_time() const { return _processing_time; }
    QueueType get_queue_type() const { return _workers_queue_ptr->identify_queue_type(); }
    void receive_package(std::unique_ptr<Package>) override;
    std::pair<ReceiverType, ElementId> verify_identity() override;

    stockpile_type_c_it begin()const override;
    stockpile_type_c_it end() const override;
    stockpile_type_c_it cbegin()const override;
    stockpile_type_c_it cend()const override;

private:
    std::optional<Package> _process_buffer = std::nullopt;
    ElementId _worker_id;
    Time _processing_time;
    std::unique_ptr<IPackageQueue> _workers_queue_ptr;
};


class Ramp : public PackageSender{
public:
    Ramp(ElementId ramp_id, Time delivery_interval, ReceiverPreferences& receiver_preferences) : PackageSender(receiver_preferences), _ramp_id(ramp_id), _delivery_interval(delivery_interval){}
    friend bool operator==(const Ramp& ramp1, const Ramp& ramp2) { return( ramp1.get_id()==ramp2.get_id() && ramp1.get_delivery_interval()==ramp2.get_delivery_interval() && ramp1.receiver_preference==ramp2.receiver_preference ); }
    void deliver_package();
    ElementId get_id() const override { return _ramp_id; }
    Time get_delivery_interval() const { return _delivery_interval; }
private:
    ElementId _ramp_id;
    Time _delivery_interval;
};


class Storehouse : public IPackageReceiver {
public:
    explicit Storehouse(ElementId storehouse_id, std::unique_ptr<IPackageStockpile> stockpile_ptr) : _storehouse_id(storehouse_id), _stockpile_ptr(std::move(stockpile_ptr)){}
    explicit Storehouse(ElementId storehouse_id) : _storehouse_id(storehouse_id), _stockpile_ptr(nullptr){}
    Storehouse(const Storehouse& orig) :  _storehouse_id(orig._storehouse_id) {}    /// DO POPRAWY! -> trzeba dodać jakoś do tego konstruktora: _stockpile_type(std::move(orig._stockpile_type))
    friend bool operator==(const Storehouse& strh1, const Storehouse& strh2) { return strh1.get_id() == strh2.get_id();}

    ElementId get_id() const { return _storehouse_id; }
    void receive_package(std::unique_ptr<Package>) override;
    std::pair<ReceiverType, ElementId> verify_identity() override;

    stockpile_type_c_it begin()const override;
    stockpile_type_c_it end() const override;
    stockpile_type_c_it cbegin()const override;
    stockpile_type_c_it cend()const override;

private:
    ElementId _storehouse_id;
    std::unique_ptr<IPackageStockpile> _stockpile_ptr;
};

/* NODES */


/* FACTORY PACKAGE */

template <class Node>
class NodeCollection {
public:
    using iterator = typename std::list<Node>::iterator;
    using const_iterator = typename std::list<Node>::const_iterator;

    explicit NodeCollection(std::list<Node> nodes_collection) : _nodes_collection(nodes_collection) {}
    //NodeCollection(const NodeCollection& orig) : _nodes_collection(orig._nodes_collection){}

    void add_node(Node &node) { _nodes_collection.push_back(std::move(node)); }

    friend bool operator== (const NodeCollection& collection1, const NodeCollection& collection2) { return collection1.get_nodes_collection()== collection2.get_nodes_collection();}

    iterator find_by_id(ElementId id)  {
        return std::find_if(_nodes_collection.begin(), _nodes_collection.end(),
                            [id](const Node& node) -> bool { return node.get_id() == id; });
    }

    const_iterator find_by_id(ElementId id) const {
        return std::find_if(_nodes_collection.begin(), _nodes_collection.end(),
                            [id](const Node& node) -> bool { return node.get_id() == id; });
    }

    void remove_by_id(ElementId id) {
        if (find_by_id(id) != _nodes_collection.end()) {
            _nodes_collection.erase(find_by_id(id));
        }
    }

    std::list<Node> get_nodes_collection() const { return _nodes_collection; }

    iterator begin() { return _nodes_collection.begin(); }
    iterator end() { return _nodes_collection.end(); }
    const_iterator cbegin() const { return _nodes_collection.cbegin(); }
    const_iterator cend() const { return _nodes_collection.cend(); }
    const_iterator begin() const { return _nodes_collection.cbegin(); }
    const_iterator end() const { return _nodes_collection.cend(); }

private:
    std::list<Node> _nodes_collection;
};



class Ramps : public NodeCollection<Ramp>{};
class Workers : public NodeCollection<Worker>{};
class Storehouses : public NodeCollection<Storehouse>{};





class Factory{
public:
    Factory(NodeCollection<Ramp>& ramps_list, NodeCollection<Worker>& workers_list, NodeCollection<Storehouse>& storehouses_list) : _ramps_list(ramps_list), _workers_list(workers_list), _storehouses_list(storehouses_list) {}
    //Factory(const Factory& orig) : _ramps_list(orig._ramps_list),  _workers_list(orig._workers_list), _storehouses_list(orig._storehouses_list) {}
    bool verify_if_consistent() const;

    NodeCollection<Ramp> get_ramps_list() const { return _ramps_list; }
    NodeCollection<Worker> get_workers_list() const { return _workers_list; }
    NodeCollection<Storehouse> get_storehouses_list() const { return _storehouses_list; }

    void delete_worker_by_id (ElementId id);
    void delete_ramp_by_id(ElementId id);
    void delete_storehouse_by_id(ElementId id);

    void add_worker(Worker& worker);
    void add_ramp(Ramp& ramp);
    void add_storehouse(Storehouse& storehouse);

    NodeCollection<Worker>::const_iterator find_worker_by_id (ElementId id) const;
    NodeCollection<Ramp>::const_iterator find_ramp_by_id(ElementId id) const;
    NodeCollection<Storehouse>::const_iterator find_storehouse_by_id(ElementId id) const;
private:
    NodeCollection<Ramp> _ramps_list;
    NodeCollection<Worker> _workers_list;
    NodeCollection<Storehouse> _storehouses_list;
    void delete_connections(IPackageReceiver* receiver_ptr);
};

/* FACTORY PACKAGE */


/* REPORT GENERATION */

class IReportNotifier{
public:
    virtual bool should_generate_report (Time time) const = 0;
};


class IntervalReportNotifier : public IReportNotifier{
public:
    IntervalReportNotifier(TimeOffset report_interval) : _report_interval(report_interval){}
    bool should_generate_report (Time time) const override;
private:
    TimeOffset _report_interval;
};


class SpecificTurnsReportNotifier : public IReportNotifier{
public:
    explicit SpecificTurnsReportNotifier(std::set<Time>& specific_turns_ids) : _specific_turns_ids(specific_turns_ids){}
    bool should_generate_report (Time time) const override;
private:
    std::set<Time> _specific_turns_ids;
};

void generate_net_structure(const Factory& factory, std::ostream report_dest);
void generate_simulation_turn_report(Time turn_number, const Factory& factory, std::ostream report_dest);

/* REPORT GENERATION */


/* SIMULATION */

void simulation(std::istream net_structure, Time rounds_n, std::ostream report_dest, const IReportNotifier& report_generation);

/* SIMULATION */


/* IO FUNCTIONS */

using struct_elem_string_map = std::map<std::string, std::map<std::string, std::string>>;
enum class StructureElementType{
    Worker,
    Ramp,
    Storehouse,
    Link
};

using struct_elem_string = std::map<StructureElementType, std::string>;
void load_structure(std::istream& input_structure);

struct_elem_string load_factory_structure(std::istream& input_structure);
void save_factory_structure(struct_elem_string& loaded_lines);   //zmien na Factory

/*    v     MOŻE SIĘ PRZYDAC     v    */


/*    ^    MOŻE SIĘ PRZYDAC      ^    */

/* IO FUNCTIONS */



#endif //DIVIDE_ET_IMPERA_DIVIDED_HPP
